function lineaire_1(popsize, Pop)
%LINEAIRE_1 Summary of this function goes here
%   Detailed explanation goes here
[~, I] = sort([Pop.fitness], 'descend');
Pop = Pop(I);
alpha = popsize/10;
beta = 2 - alpha;

for n = 1:popsize
    rk = n - 1;
    Pop(n).selec_proba = [(alpha+((rk*(beta - alpha))/(popsize - 1)))/popsize , 0];
end
end

