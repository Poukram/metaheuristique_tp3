function rel_fitness(popsize, Pop)
%REL_FITNESS Assigns a probability to be selected
% Uses relative fitness values
fitness_sum = sum([Pop.fitness]);

% First compute pi
for n = 1:popsize
    Pop(n).selec_proba = [ Pop(n).fitness / fitness_sum , 0]; % as [pi , pci]
end

end

