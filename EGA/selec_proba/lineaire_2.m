function lineaire_2(popsize, Pop)
%LINEAIRE_2 Summary of this function goes here
%   Detailed explanation goes here
[~, I] = sort([Pop.fitness], 'descend');
Pop = Pop(I);
r = (2/(N*(N-1)))/4;
q = (r*(N-1))/2 + 1/N;

for n = 1:popsize
    Pop(n).selec_proba = [q - (n - 1) * r, 0];
end
end