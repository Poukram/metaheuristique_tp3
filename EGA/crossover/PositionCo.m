function enfants = PositionCo(Pop, popsize, pc, pb)
%PARTIAL_CO Summary of this function goes here
%   Detailed explanation goes here

enfants(popsize) = Individual;
perm = randperm(popsize);
n = 0;

while(n < popsize)
    p1 = Pop(perm(n + 1));
    p2 = Pop(perm(n + 2));
    ps = [[p1 p2];[p2 p1]];
    
    tmp = rand();
    
    if(tmp <= pc)
        for i = 1:2
            enfants(n + i).towns_perm = zeros(1, pb.nb_towns);
            
            sel = randperm(pb.nb_towns, randi(pb.nb_towns));
            ps1 = ps(i,1);
            ps2 = ps(i,2);
            
            enfants(n + i).towns_perm(sel) = ps1.towns_perm(sel);
            
            sel = ~ismember(ps2.towns_perm, enfants(n + i).towns_perm);
            sel = ps2.towns_perm(sel);
            
            lid = enfants(n + i).towns_perm == 0;
            enfants(n + i).towns_perm(lid) = sel;
        end
    else
        enfants(n + 1).towns_perm = p1.towns_perm;
        enfants(n + 2).towns_perm = p2.towns_perm;
    end
    n = n +2;
end
end

