function enfants = PartialCo(Pop, popsize, pc, pb)
%PARTIAL_CO Summary of this function goes here
%   Detailed explanation goes here

% FIXME not working yet because of the mechanism to rewrite non cut parts
% of the permutation..
enfants(popsize) = Individual;
perm = randperm(popsize);
n = 0;

while(n < popsize)
    p1 = Pop(perm(n + 1));
    p2 = Pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        ps = [[p1 p2];[p2 p1]];
        cp = randperm(pb.nb_towns, 2);
        cp = sort(cp);
        cut_len = cp(2) - cp(1) + 1;
        
        for i = 1:2
            enfants(n + i).towns_perm = zeros(1, pb.nb_towns);
            ps1 = ps(i,1);
            ps2 = ps(i,2);
            
            
            enfants(n + i).towns_perm(cp(1):cp(2)) = ps1.towns_perm(cp(1):cp(2));
            cut = ps2.towns_perm(cp(1):cp(2));
            
            p=ismember(cut,enfants(n + i).towns_perm);
            not_cp = cut(~p);
            
            for k = not_cp
                A = k;
                while 1
                    m1 = ps2.towns_perm == A;
                    V = ps1.towns_perm(m1);
                    m2 = find(ps2.towns_perm == V);
                    
                    if m2 < cp(1) || m2 > cp(2)
                        break;
                    end
                    A = V;
                end
                enfants(n + i).towns_perm(m2) = k;
            end
            
            k = find(~enfants(n + i).towns_perm);
            if (numel(k) > 0)
                for m = 1:numel(k)
                    enfants(n + i).towns_perm(k(m)) = ps2.towns_perm(k(m));
                end
            end
        end
    else
        enfants(n + 1).towns_perm = p1.towns_perm;
        enfants(n + 2).towns_perm = p2.towns_perm;
    end
    n = n +2;
end
end

