function enfants = CycleCo(Pop, popsize, pc, pb)
%PARTIAL_CO Summary of this function goes here
%   Detailed explanation goes here

% FIXME not working yet because of the mechanism to rewrite non cut parts
% of the permutation..
enfants(popsize) = Individual;
perm = randperm(popsize);
n = 0;

while(n < popsize)
    p1 = Pop(perm(n + 1)).towns_perm;
    p2 = Pop(perm(n + 2)).towns_perm;
    
    tmp = rand();
    
    if(tmp <= pc)
        pss = [[p1;p2];[p2;p1]];
        
        enfants(n + 1).towns_perm = zeros(1, pb.nb_towns);
        enfants(n + 1).towns_perm = zeros(1, pb.nb_towns);
        
        t1 = zeros(1, pb.nb_towns);
        t2 = zeros(1, pb.nb_towns);
        
        currid = 1;
        order = 1;
        
        while 1 % find all cycles
            if order == 1
                order = 0;
            else
                order = 1;
            end
      
            ps1 = pss(1 + (order * 2),:);
            ps2 = pss(1 + (order * 2) + 1,:);
            
            orig = ps1(currid);
            
            while 1 % go through the current cycle   
                if order == 0
                    t1(currid) = ps1(currid);
                    t2(currid) = ps2(currid);
                else
                    t2(currid) = ps1(currid);
                    t1(currid) = ps2(currid);
                end
                
                if ps2(currid) == orig
                    break;
                end
                
                currid = find(ps1 == ps2(currid));
            end
            t = find(t1 == 0);
            
            if (numel(t) == 0)
                break;
            end
            
            currid = t(1);
        end
        enfants(n + 1).towns_perm = t1;
        enfants(n + 2).towns_perm = t2;
    else
        enfants(n + 1).towns_perm = p1;
        enfants(n + 2).towns_perm = p2;
    end
    n = n +2;
end
end

