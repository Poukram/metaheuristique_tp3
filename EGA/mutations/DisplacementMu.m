function  DisplacementMu(Pop, pm, pb)
%SWITCHMU Same as Inversion but without reversing

for p = Pop
    tmp = rand();
    if (tmp <= pm)
        sel = [0 0];
        sel(1) = randi(pb.nb_towns);
        
        if (sel(1) == pb.nb_towns)
            sel(2) = sel(1);
        else
            sel(2) = sel(1) + randi(pb.nb_towns - sel(1));
        end
        
        s = p.towns_perm(sel(1):sel(2));
        l = ~ismember(p.towns_perm, s);
        a = p.towns_perm(l);
        
        l = numel(a);
        if (l == 0)
            continue;
        end
        
        pos = randi(l);
        
        if (pos == 1)
            p.towns_perm = [s a];
        elseif (pos == numel(a))
            p.towns_perm = [a s];
        else
            p.towns_perm = [a(1:pos-1) s a(pos:end)];
        end
    end
end
end

