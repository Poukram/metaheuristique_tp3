function  SwitchMu(Pop, pm, pb)
%SWITCHMU Selects two locuses to switch

for p = Pop
    tmp = rand();
    if (tmp <= pm)
        % randperms allows to select two points in a 1:N vector in one call
        to_switch = randperm(pb.nb_towns, 2);
        
        tmp = p.towns_perm(to_switch(2));
        p.towns_perm(to_switch(2)) = p.towns_perm(to_switch(1));
        p.towns_perm(to_switch(1)) = tmp;
    end
end
end

