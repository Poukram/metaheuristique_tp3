function  InversionMu(Pop, pm, pb)
%INVERSIONMU Selects a chunck of gene and moves it after reversing it

for p = Pop
    tmp = rand();
    if (tmp <= pm)
        sel = [0 0];
        sel(1) = randi(pb.nb_towns); % The locus where to start
        
        if (sel(1) == pb.nb_towns)
            sel(2) = sel(1); % if we use last locus then len has to be 1
        else
            % length of the portion of gene to use
            sel(2) = sel(1) + randi(pb.nb_towns - sel(1));
        end
        % Select and reverse
        s = p.towns_perm(sel(1):sel(2));
        s = wrev(s);
        
        % Identify not selected
        l = ~ismember(p.towns_perm, s);
        a = p.towns_perm(l);
        
        % If all have been selected no need to go further
        l = numel(a);
        if (l == 0)
            continue;
        end
        
        % The locus of insertion
        pos = randi(l);
        
        if (pos == 1)
            p.towns_perm = [s a];
        elseif (pos == numel(a))
            p.towns_perm = [a s];
        else
            % insert around the unselected
            p.towns_perm = [a(1:pos-1) s a(pos:end)];
        end
    end
end
end

