function  InsertionMu(Pop, pm, pb)
%INSERTIONMU Same as inversion with only one locus

for p = Pop
    tmp = rand();
    if (tmp <= pm)
        sel = randperm(pb.nb_towns, 2);
        s = p.towns_perm(sel(1));
        a = p.towns_perm(p.towns_perm ~= s);
        
        if (sel(2) == 1)
            p.towns_perm = [s a];
        elseif (sel(2) == pb.nb_towns)
            p.towns_perm = [a s];
        else
            p.towns_perm = [a(1:sel(2)-1) s a(sel(2):end)];
        end
    end
end
end

