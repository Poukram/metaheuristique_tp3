function linear_scale(Pop, popsize)
%LINEAR_SCALE Summary of this function goes here
%   Detailed explanation goes here
pop_fit = [Pop.fitness];
fit_min = min(pop_fit);
fit_avg = mean(pop_fit);
fit_max = max(pop_fit);
c1 = fit_avg/(fit_avg - fit_min);
c2 = (fit_avg * fit_min)/(fit_max - fit_min);

for n = 1:popsize
    Pop(n).fitness = c1 * Pop(n).fitness + c2;
end

end

