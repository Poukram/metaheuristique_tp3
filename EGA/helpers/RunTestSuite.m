function RunTestSuite(base_param)
%RUNTESTSUITE Builds and runs test suite then saves results in spreadsheet
%   Paralleleld execution over the configurations to limit overhead time

crossovers_names = ["partiel", "position", "cycle"];
mutations_names = ["echange", "insertion", "deplacement", "inversion"];
excel_file = "test_data.xlsx";
var_names = {"Croisement", "Mutation", "Meilleure distance",...
    "Distance moyenne", "Variance distance"};
repetitions = 100;

confs = repmat(base_param,1, 9);
confs(1).crossover = crossovers_names(1);
confs(1).mutation = mutations_names(1);

confs(2).crossover = crossovers_names(1);
confs(2).mutation = mutations_names(3);

confs(3).crossover = crossovers_names(1);
confs(3).mutation = mutations_names(4);

confs(4).crossover = crossovers_names(2);
confs(4).mutation = mutations_names(1);

confs(5).crossover = crossovers_names(2);
confs(5).mutation = mutations_names(3);

confs(6).crossover = crossovers_names(2);
confs(6).mutation = mutations_names(4);

confs(7).crossover = crossovers_names(3);
confs(7).mutation = mutations_names(1);

confs(8).crossover = crossovers_names(3);
confs(8).mutation = mutations_names(3);

confs(9).crossover = crossovers_names(3);
confs(9).mutation = mutations_names(4);

res = cell(numel(confs)+1, 5);
res(1,:) = var_names;

parfor i = 1:numel(confs)
    p = confs(i);
    best_dist = Inf;
    amd = zeros(1, repetitions);
    avd = zeros(1, repetitions);
    
    for k = 1:repetitions
        r = TSMGA(p);
        g = r.hist(r.gen_nb + 1,:);
        [sd, ~] = sort([g.dist], 'ascend');
        
        amd(k) = mean(sd);
        avd(k) = var(sd);
        
        if best_dist > sd(1)
            best_dist = sd(1);
        end
    end

    res(i+1,:) = {p.crossover p.mutation best_dist mean(amd) mean(avd)};
end
str_tabl = string(res(:,:)); % convert all data to string to make sure they write ok
xlswrite(excel_file, str_tabl, "Traveling Salesman Problem", "A1");zeros(1, repetitions);
end

