function sigma_trunc(Pop, popsize)
%SIGMA_TRUNC Summary of this function goes here
%   Detailed explanation goes here
pop_fit = [Pop.fitness];
c = 2.5;
sigma = std(pop_fit);
fit_avg = mean(pop_fit);

for n = 1:popsize
    tmp = Pop(n).fitness - (fit_avg - c * sigma);
    if (tmp < 0)
        tmp = 0;
    end
    Pop(n).fitness = tmp;
end
end