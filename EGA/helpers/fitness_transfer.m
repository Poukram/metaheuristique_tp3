function fitness_transfer(Pop, popsize, pb_type)
%FITNESS_TRANSFER Summary of this function goes here
%   Detailed explanation goes here

if (pb_type == 0)
    max_fitness = max([Pop.fitness]);
    for n = 1:popsize
        Pop(n).fitness = max_fitness - Pop(n).fitness;
    end
else
    min_fitness = min([Pop.fitness]);
    if (min_fitness < 0)
        for n = 1:popsize
            Pop(n).fitness = Pop(n).fitness - min_fitness;
        end
    end
end

