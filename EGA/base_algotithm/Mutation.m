function Mutation(mutation, Pop, pm, pb)
%MUTATION This function determines which mutation operator to use

switch mutation
    case "echange"
        SwitchMu(Pop, pm, pb);
    case "insertion"
        InsertionMu(Pop, pm, pb);
    case "deplacement"
        DisplacementMu(Pop, pm, pb);
    case "inversion"
        InversionMu(Pop, pm, pb);
    otherwise
        warning("Unexpected mutation name, maybe not yet implemented");
end
end