function Pop = InitPop(popsize, pb)
%INIT_POP initializes the population for the genetic algorithm

Pop(popsize) = Individual;
for p = Pop
    p.towns_perm = randperm(pb.nb_towns);
end
end
