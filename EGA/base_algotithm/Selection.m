function mating_pool = Selection(Pop, param)
%SELECTION This function handles the selection and building of the mating
%pool
%   It first computes the probability that an individual will be chosen for
%   mating then uses the given selection method to build a mating pool;

% First compute pi
switch param.proba_sel
    case "relative"
        rel_fitness(param.popsize, Pop); % compute the probability of being chosen
    case "lineaire_1"
        lineaire_1(param.popsize, Pop);
    case "lineaire_2"
        lineaire_1(param.popsize, Pop);
    case "non_lineaire"
        non_lineaire(param.popsize, Pop);
    otherwise
        warning("Warning unsupported selection probability method")
end

%Then compute pci
for n = 1:param.popsize
    pci = 0;
    if (n == 1)
        pci = Pop(n).selec_proba(1);
    else
        for k = 1:n
            pci = pci + Pop(k).selec_proba(1);
        end
    end
    Pop(n).selec_proba(2) = pci;
end

% Then build mating pool
switch param.selection
    case "RWS"
        mating_pool = rws(Pop, param.popsize);
    case "SUS"
        mating_pool = sus(Pop, param.popsize);
    case "tournoi"
        mating_pool = tournoi(Pop, param.popsize);
    otherwise
        warning("Bad selection mode, aborting")
end
end

