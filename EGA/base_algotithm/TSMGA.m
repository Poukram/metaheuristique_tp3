function res = TSMGA(param)
%TSMGA A Genetic Algorithm adapted for Traveling Salesman Problem
%   Chooses the operators to call depending on user settings
%   and returns a structure containing all history data to plot

%--------------Init vars -------------------------------------------------
pb = param.pb;
pop_history(param.g_max+1, param.popsize) = Individual;
gen_nb = 0;
alive = 1;
%-------------------------------------------------------------------------
P = InitPop(param.popsize, pb);
EvalFitness(P, pb, param);

pop_history(1,:) = P;

while (alive && gen_nb < param.g_stop)
    if (param.steady_s == 1)
       P1 = SteadyS(P, param, gen_nb);
    else
        mating_pool = Selection(P, param);
        P1 = Croisement(param.crossover, mating_pool, param.popsize, param.pc, pb);
        Mutation(param.mutation, P1, param.pm, pb);
    end
    EvalFitness(P1, pb, param);
    P = P1;
    
    gen_nb = gen_nb + 1;
    pop_history(gen_nb+1,:) = copy(P);
    alive = Stopping(param, param.popsize, pop_history, gen_nb, param.g_max);
end
res = struct;
res.hist = pop_history;
res.popsize = param.popsize;
res.gen_nb = gen_nb;
res.pb = pb;
end

