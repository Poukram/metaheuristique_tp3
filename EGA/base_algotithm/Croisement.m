function enfants = Croisement(crossover, pop, popsize, pc, pb)
%CROISEMENT This function determines which crossover operator to use

switch crossover
    case "partiel"
        enfants = PartialCo(pop, popsize, pc, pb);
    case "position"
        enfants = PositionCo(pop, popsize, pc, pb);
    case "cycle"
        enfants = CycleCo(pop, popsize, pc, pb);
    otherwise
        warning("Unexpected crossover name, maybe not yet implemented");
end
end

