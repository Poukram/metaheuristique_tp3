function EvalFitness(Pop, pb, param)
%EVAL_FITNESS This functions evaluates and scales fitness
%   Fitness scaling depends on the algorithm's parameters

pb.PopFitnessCalc(Pop);
fitness_transfer(Pop, param.popsize, param.pb_type);

switch param.scale
    case "none"
        return;
    case "lineaire"
        linear_scale(Pop, param.popsize);
    case "sigma"
        sigma_trunc(Pop, param.popsize);
    otherwise
        warning('Used incorrect scaling method')
end
end

