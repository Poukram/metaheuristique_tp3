function mating_pool = rws(Pop, popsize)
%RWS Performs a roulette wheel selection in a pool of individuals

mating_pool(popsize) = Individual;

selected = Individual;

for n = 1:popsize
    tmp = rand();
    k = 1;

    if (tmp == 1)
        selected = Pop(popsize);
        continue;
    end
    
    while ( k <= popsize)
        if (k == 1 && Pop(k).selec_proba(2) > tmp)
            selected = Pop(1);
            break;
        elseif (k > 1 && Pop(k-1).selec_proba(2) <= tmp && Pop(k).selec_proba(2) > tmp)
            selected = Pop(k);
            break;
        end
        k = k + 1;
        
        if (k > popsize)
            break;
        end
    end
    
    mating_pool(n) = selected;            
end

end

