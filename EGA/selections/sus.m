function mating_pool = sus(Pop, popsize)
%SUS This performs a N pointers Stochastich Universal Sampling

mating_pool(popsize) = Individual;
nb_pointers = 4;
selected = Individual;

for n = 1:popsize/nb_pointers
    u = rand()/nb_pointers;
    for i = 1:nb_pointers
        tmp = u*i;
        k = 1;
        
        if (tmp == 1)
            selected = Pop(popsize);
            continue;
        end
        
        while ( k <= popsize)
            if (k == 1 && Pop(k).selec_proba(2) > tmp)
                selected = Pop(1);
                break;
            elseif (k > 1 && Pop(k-1).selec_proba(2) <= tmp && Pop(k).selec_proba(2) > tmp)
                selected = Pop(k);
                break;
            end
            k = k + 1;
            
            if (k > popsize)
                warning("Could not find a valid individual")
                break;
            end                        
        end
        count = (n - 1) * nb_pointers + i;
        if (count > popsize)
            break;
        end
        mating_pool(count) = selected; 
    end
end

end

