function mating_pool = tournoi(Pop, popsize)
%TOURNOI This performs a non biased k turns tournoi selection

mating_pool(popsize) = Individual;
t_size = 8; % The size of the tournoi
t = zeros(t_size, popsize);

for n = 1:t_size
    t(n,:) = randperm(popsize);
end

tmp(1, t_size) = Individual;
for n = 1:popsize
    for k = 1:t_size
        tmp(k) = Pop(t(k,n));
    end
    [~,I] = max([tmp.fitness]);
    mating_pool(n) = tmp(I);
end
end

