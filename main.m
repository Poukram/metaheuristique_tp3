%------------Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))
%---------------------------------------------------------------------

%--------------Configuration of operators and misc variables----------
crossovers_names = ["partiel", "position", "cycle"];
mutations_names = ["echange", "insertion", "deplacement", "inversion"];
sel_prob_type = ["relative", "lineaire_1", "lineaire_2", "non_lineaire"];
selection_type = ["RWS", "SUS", "tournoi"];
chgt_echelle_type = ["none", "lineaire", "sigma"];
stopping_type = [ "temps", "fitness", "tx_chgt_fitness"];
param.pb_type = 1; % Minimization is 1 max is 0
param.steady_s = 0;

param.crossover = crossovers_names(3);
param.mutation = mutations_names(4);
param.proba_sel = sel_prob_type(3);
param.selection = selection_type(3);
param.scale = chgt_echelle_type(2);
param.critere_arret = stopping_type(3);

param.nb_towns = 6;
param.pb = TSMProblem(param.nb_towns);
%---------------------------------------------------------------------

%--------------Configuration of the genetic algorithm----------
param.popsize = 500;
param.g_max = 600; % The maximum number of generations
param.g_stop = 1000; % If we reach this generation nmber we stop no matter what
param.pc = 0.50; % The probability of a crossover to happen
param.pm = 0.175; % The probability of a mutation to happen
%---------------------------------------------------------------------

%------------Call to the genetic algorithm-----------
r = TSMGA(param);
g = r.hist(r.gen_nb + 1,:);
[sd, I] = sort([g.dist], 'ascend');
g = g(I);
g(1).PlotCycle(param.pb);
%---------------------------------------------------------------------