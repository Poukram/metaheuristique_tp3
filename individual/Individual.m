classdef Individual < matlab.mixin.Copyable
%INDIVIDUAL Stores information about an individual
%   This instance has been adapted for the TSM problem    
    properties
        towns_perm
        dist
        fitness
        selec_proba
    end
    
    methods
        function obj = Individual(towns_perm, fitness)
            %INDIVIDUAL Construct an instance of this class
            %   Can be constructed without input arguments to create arrays
            if (nargin == 0)
                towns_perm = [];
                fitness = 0;
            end
            obj.towns_perm = towns_perm;
            obj.dist = 0;
            obj.fitness = fitness;
            obj.selec_proba = 0;
        end
        % An interface to display the solution held by an individual
        function PlotCycle(obj, pb)
            t = transpose(pb.towns(obj.towns_perm,:));
            t = [t t(:,1)];
            figure;
            line(t(1,:), t(2,:));
            hold on;
            % towns points are super-imposed on the cycle plotting
            scatter(t(1,:), t(2,:), 65, 'MarkerEdgeColor',[0 0 0],...
                'MarkerFaceColor',[1 0 0], 'LineWidth',1.5);
            title("Cycle distance : " + obj.dist);
            hold off;
        end
    end
end

