classdef TSMProblem
%TSMPROBLEM This class holds an instance of the TSM problem
%   gives access to the number of towns, their coordinates
%   and the distance between them.
    
    properties
        nb_towns
        towns
        towns_dist
    end
    
    methods
        function obj = TSMProblem(nb_towns)
            if (nargin == 0)
                nb_towns = 4;
            end
            obj.nb_towns = nb_towns;
            obj.towns = rand(nb_towns, 2);
            obj.towns_dist = pdist2(obj.towns, obj.towns);
        end
        
        % Computes the fitness of all a population
        function PopFitnessCalc(obj, Pop)
            % We want to be sure we deal with an array of the correct type
            if isa(Pop, 'Individual')
                all_perms = vertcat(Pop.towns_perm);
                for i = 1:numel(Pop)
                    pairs = [all_perms(i,1:end-1); all_perms(i,2:end)];
                    dist_p = zeros(1, obj.nb_towns - 1);
                    for k = 1:obj.nb_towns - 1
                        dist_p(k) = obj.towns_dist(pairs(1,k), pairs(2,k));
                    end
                    % The distance of the cycle is used for fitness
                    Pop(i).fitness = sum(dist_p);
                    Pop(i).dist = Pop(i).fitness;
                end
            end
        end
    end
end

